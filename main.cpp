#include <iostream>
#include <string>
#include <sstream>

#include <ncurses.h>
#include <functional>

#include <chrono>

#include <thread>

#include <queue>

#include "keys.h"

typedef void * raw_data;
typedef char *             cstring;
typedef unsigned char      u8;
typedef char               s8;
typedef short              s16;
typedef unsigned short     u16;
typedef int                s32;
typedef unsigned int       u32;
//typedef __int64            s64;
//typedef unsigned __int64   u64;

#define TIME std::chrono::system_clock::now();

// TODO: Cl�ear this shit ...
using namespace std;
using namespace std::chrono;
using namespace std::chrono_literals;


const char HEAD_CHAR = 'X';
const char BODY_CHAR = 'O';

char moveAndReadCharAt(u32 x, u32 y)
{
  return mvinch(y, x) & A_CHARTEXT;
}

// Timer stuff
class Timer
{
public:
  using seconds = duration<double>;
  using milliseconds = duration<double, std::ratio_multiply<seconds::period, milli>>;
  using microseconds = duration<double, std::ratio_multiply<seconds::period, micro>>;

  Timer() = default;
  ~Timer() = default;

  float getDeltaTime();
  void init();
  void tick();
  void endFrame();

private:
  system_clock::time_point currentTime;
  system_clock::time_point lastTime;
};

float Timer::getDeltaTime()
{
  duration<float, milli> dt = currentTime - lastTime;
  return dt.count();
}

void Timer::init()
{
  currentTime = TIME
  lastTime = TIME
}

void Timer::tick()
{
  lastTime = currentTime;
  currentTime = TIME
}

// Math / Collision
struct Point
{

  u32 X;
  u32 Y;

  Point() : X(0), Y(0) {}
  Point(u32 x, u32 y) : X(x), Y(y) {}
};

struct Rectangle
{
  u32 X;
  u32 Y;
  u32 Width;
  u32 Height;

  Rectangle() : X(0), Y(0), Width(0), Height(0) {}
  Rectangle(int x, int y, int w, int h) : X(x), Y(y), Width(w), Height(h) {}
};


WINDOW* mainWindow;

class Entity
{
public:

protected:
  Point coord;
  char currentChar;
  char charBeingSteppedOn;

  public:
    void setPosition(u32 x, u32 y) { 
      coord.X = x; 
      coord.Y = y; 
    }

    void setCurrentChar(char currentChar) { this->currentChar = currentChar; }
    void setCharBeingSteppedOn(char charBeingSteppedOn) { this->charBeingSteppedOn = charBeingSteppedOn; }

    Point & getPosition() { return coord; }
    char getCurrentChar() { return currentChar; }
    char getCharBeingSteppedOn() { return charBeingSteppedOn; }

    void moveTo(u32 x, u32 y);
};

void Entity::moveTo(u32 x, u32 y)
{
  mvaddch(coord.Y, coord.X, charBeingSteppedOn);
  charBeingSteppedOn = moveAndReadCharAt(coord.X + x, coord.Y + y);
  coord.X += x;
  coord.Y += y;
  move(coord.Y, coord.X);
  addch(currentChar);
}

class SnakeHead : public Entity
{
public:
  void moveOneStep();
  void increaseBody();
private:
  Point direction;
  // Performance chaos begins here, no pool allocation :(
  std::queue<Entity> body;
public:
  void setDirection(u32 x, u32 y) { direction.X = x; direction.Y = y; }
};

void SnakeHead::moveOneStep()
{
  // Ncurses needs to have ites head at position where to write.
  move(coord.Y, coord.X);
  moveTo(direction.X, direction.Y);
}

void SnakeHead::increaseBody()
{
  auto front = body.front();
  if(front != body.end())
  {
    front.setCurrentChar(BODY_CHAR);
    front.setCharBeingSteppedOn(getCharBeingSteppedOn());

    Entity newBodyPart;


  }
}

// Ncurses


// Game specifics

void draw();
void update(float dt);

void gatherInput();
void handleInput(int key);

void refreshMainWindow()
{
  refresh();
}

void clearWindow(WINDOW* window)
{
  wclear(window);
}


// Okay, I know this shit is ugly, don't even mention it....

const int maxActionCount = 10;
int actionCounter = 0;
int keyActions[maxActionCount];

int width = 0;
int height = 0;

float moveCounter = 0.0f;
float currentX = 0.0f;

void moveDebug(s32 toX, s32 toY, s32 fromX, s32 fromY)
{
  move(fromX, fromY);
  addstr(" ");
  move(toX, toY);
  addstr("X");
}

Rectangle bounds;
Point head;
Point direction;

SnakeHead snakeHeadTest;


void drawRectangle(const Rectangle & rectangle, char block)
{
  for(int i = 0; i < rectangle.Width; i++)
  {
    move(rectangle.Y, rectangle.X + i);
    addch(block);

    move(rectangle.Y + rectangle.Height, rectangle.X + i);
    addch(block);
  }

  for(int i = 0; i < rectangle.Height; i++)
  {
    move(rectangle.Y + i, rectangle.X);
    addch(block);

    move(rectangle.Y + i, rectangle.X + rectangle.Width);
    addch(block);
  }
}

bool isInside(const Rectangle& rectangle, const Point point)
{
  if(point.X < rectangle.X)
    return false;
  if(point.X > rectangle.X + rectangle.Width)
    return false;
  if(point.Y < rectangle.Y)
    return false;
  if(point.Y > rectangle.Y + rectangle.Height)
    return false;

  return true;
}

void handleInput(s32 key);

int main(void)
{
  Timer timer;

  bool running = true;
  float totalMilliSeconds = 0.0f;

  // Clean this up a little?
  mainWindow = initscr();
  noecho();
  cbreak();
  nodelay(mainWindow, true);
  getmaxyx(mainWindow, height, width);
  curs_set(0);


  bounds.X = 5;
  bounds.Y = 5;
  bounds.Width = width - 10;
  bounds.Height = height - 10;

  head.X = bounds.X + 1;
  head.Y = 15;

  direction.X = 1;
  direction.Y = 0;


  snakeHeadTest.setPosition(bounds.X + 1, 15);
  snakeHeadTest.setDirection(1, 0);
  snakeHeadTest.setCurrentChar(HEAD_CHAR);
  snakeHeadTest.setCharBeingSteppedOn(' ');
  

  drawRectangle(bounds, 'H');

  // Because writing !mainWindow is for n00bs
  if(mainWindow == nullptr)
  {
    std::cout << "Could not start window" << std::endl;
    return 0;
  }

  timer.init();
  while(running)
  {
    gatherInput();
    timer.tick();
    float dt = timer.getDeltaTime();
    update(dt);
    draw();

    moveCounter += dt;
    totalMilliSeconds += dt;

    if(moveCounter > 300.0f)
    {
      snakeHeadTest.moveOneStep();
      moveCounter = 0.0f;
    }

    running = isInside(bounds, head);

    if(actionCounter > 0)
    {
      if(keyActions[0] == 120)
      {
        running = false;
      }
    }



    move(1, 1);
    std::stringstream deltaMsg;
    deltaMsg << "DeltaTime: " << totalMilliSeconds;
    addstr(deltaMsg.str().c_str());
    move(2, 1);
    std::stringstream boundsMsg;
    
    boundsMsg << "Width: " << width << " Height: " << height << " Actions in the buffer: " << actionCounter;
    addstr(boundsMsg.str().c_str());

    actionCounter = 0;

    
  }

  std::cout << "Program ended..." << std::endl;
  endwin(); 
  return 0;
}

void draw()
{
  for(int i = 0; i < maxActionCount; i++)
  {
    move(4 + i, 1);
    addstr("   ");
  }

  for(int i = 0; i < actionCounter; i++)
  {
    move(4 + i, 1);
    addstr(std::to_string(keyActions[i]).c_str());
  }

  //move(20, 5);
  //addstr("Mooooooooo");

}

void update(float dt)
{
}

void gatherInput()
{
  int character;
  while((character = getch()) != ERR)
  {
    keyActions[actionCounter] = character;
    actionCounter++;
    if(actionCounter >= maxActionCount)
    {
      break;
    }
  }
  for(int i = 0; i < actionCounter; i++)
  {
    handleInput(keyActions[i]);
  }
}

void handleInput(s32 key)
{
  move(3, 1);

  if(key == 65)
  {
    addstr("Moved up");
    snakeHeadTest.setDirection(0, -1);
  }
  if(key == 66)
  {
    addstr("Moved down");
    snakeHeadTest.setDirection(0, 1);
  }
  if(key == 68)
  {
    addstr("Moved left");
    snakeHeadTest.setDirection(-1, 0);
  }
  if(key == 67)
  {
    addstr("Moved right");
    snakeHeadTest.setDirection(1, 0);
  }
}
